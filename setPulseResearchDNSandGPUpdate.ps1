if( Get-DAConnectionStatus ){
    Write-Output "Computer is connected to DirectAccess Server. Continuing..."
}

$VPNAdaptor = Get-NetAdapter | where {$_.InterfaceDescription -like "Juniper*"}
if( $VPNAdaptor.Status -eq "Up"){
    Write-Output "VT VPN is connected, setting RESEARCH DNS..."
    $VPNAdaptor | Set-DnsClientServerAddress -ServerAddresses 128.173.17.8,198.82.152.157
    $VPNAdaptor | Set-DnsClientServerAddress -ServerAddresses 2001:468:c80:c11f:0:50a7:80ad:1108,2001:468:c80:2133:0:5062:c652:989d
    Clear-DnsClientCache
} else {
    Write-Output "VT VPN is not connected, please connect and re-run script. Exiting..."
    break
}

Write-Output "Waiting for DNS to update..."
Start-Sleep -Seconds 30

if(nltest /dsgetdc:){
    Write-Output "Domain connection successful, applying GPO..."
    gpupdate /force
    if($?){
        Write-Output "Group Policy Update Succeeded. Please reboot the machine!"
#        $VPNAdaptor | Set-DnsClientServerAddress -ResetServerAddresses
        break
    } else {
        Write-Output "Group Policy Update Failed. Exiting..."
        break
    }
} else {
    Write-Output "Domain connection failed, exiting..."
    break
}
